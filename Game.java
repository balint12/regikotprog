/* **************************************************************************
 * H A N D R � M I
 * Prog I, 1. k�telez� program: handr�mi megval�s�t�sa.
 * K�sz�tette: B�k�si B�lint 
 * EHA: BEBSAAT.SZE
 ****************************************************************************/
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;
import java.util.*;
import javax.swing.*;


/**
 * Ez az oszt�ly a b�zis, minden ebb�l indul ki, 
 * itt helyezkedik el a static main is.
 * @author      B�lint B�k�si <Bekesi.Balint @ stud.u-szeged.hu>
 */
public class Game {

	protected static Control c;
	protected static Draw d;

	private LinkedList <Kartyalap>asztal = new LinkedList<Kartyalap>();
	private LinkedList <Kartyalap>pakli = new LinkedList<Kartyalap>();
	private int k�r;
	private int jatszma;
	
	private Player[] players;
	private int players_num;
	private int oszto;
	
	public Kartyalap asztalrolKivesz( Kartyalap mit ) {
		asztal.remove(mit);
		return mit;
	}
	
	public void asztalraHelyez( Kartyalap mit ) {
		asztal.add(mit);
	}
	
	public void paklibaHelyez( Kartyalap mit ) {
		pakli.add(mit);
	}
	
	public Kartyalap paklibolHuz() {
		Kartyalap first = pakli.getFirst();
		pakli.remove();
		return first;
	}	
	
	/** 
	 * Konstruktor
	 * @param players_num A j�t�kosok sz�ma
	 */
	public Game( int players_num ) {
		// j�t�kosok k�sz�t�se
		this.players_num = players_num;
		players = new Player[players_num];
		for(int i=0; i<players_num; i++) {
			players[i] = new Player((i+1)+" jatekos");
		}
		oszto=0;
		//pakli k�sz�t�se
		for(int szin=1; szin<=4; szin++) {
			for(int jel=1; jel<=13; jel++) {
				paklibaHelyez(new Kartyalap(szin,jel));
				paklibaHelyez(new Kartyalap(szin,jel));
			}
		}
		//oszt�s
		for(Player p: players) {
			for(int oo=0;oo<14;oo++) {
				p.KezbeHelyez(paklibolHuz());
			}
		}
		//megmutatja
		//d.update();
		
		
		while(true) {}
	}

	/** kiindul�si pont */
	public static void main(String[] args) {

		JFrame f = new JFrame("Handr�mi j�t�k");
		
		c = new Control(f);
		d = new Draw(f);
		
		Game g = new Game(4);

	}
}


/**
 * K�rtyalap oszt�ly
 * @author      B�lint B�k�si <Bekesi.Balint @ stud.u-szeged.hu>
 */
class Kartyalap {

		private int szin;
		private int jel;
		
		/** 
		 * Konstruktor
		 * @param szin A k�rtya sz�ne
		 * @param jel A k�rtya jele
		 */
		public Kartyalap(int szin, int jel) {
			this.szin = szin;
			this.jel = jel;
		}
		
		/** 
		 * A k�rtya sz�n�nek lek�rdez�se
		 * @return A k�rtya sz�ne
		 */
		public int getSzin() {
			return szin;
		}
		
		/** 
		 * A k�rtya jel�nek lek�rdez�se
		 * @return A k�rtya jele
		 */
		public int getJel() {
			return jel;
		}
		
		/** 
		 * A lap �rt�k�t k�rdezhetj�k le 
		 * @return A lap �rt�ke
		 */
		public int getErtek() {
			if(jel==13) return 11;
			if(jel<=10) return jel;
			return 10;
		}
	}


/**
 * K�l�n oszt�ly a Graphics parancsokra, itt t�roljuk el a bet�lt�tt k�pek
 * v�ltoz�it is.
 * @author      B�lint B�k�si <Bekesi.Balint @ stud.u-szeged.hu>
 */
class Draw extends Component {
	
    BufferedImage kartya;
	Font nagyBetu;
	
	/**
	 * konstruktor, ekkor t�lti be a sz�ks�ges grafikus elemeket.
	 */
	public Draw( JFrame f ) {
		f.add(this);
		f.pack();
		f.setVisible(true);
		kartya = load("kartya.jpg");
		nagyBetu = new Font("Times New Roman", Font.PLAIN, 50);
	}
	
	/**
	 * Megjelen�ti a k�pet
	 */
	public void paint(Graphics g) {
		g.drawImage(kartya, 0, 0, null);
		g.setFont(nagyBetu);
		g.drawString("�dv�z�llek a Handr�mi j�t�kban",100,200);
		System.out.println("hopszi");
	}

	/** �tm�retez�s getPreferredSize() seg�ts�g�vel. */
	public Dimension getPreferredSize() {
		return new Dimension(700,400);
	}
	
	/** 
	 * A k�pek bel�lt�s�t egyszer�s�t� f�ggv�ny
	 * @param filename A k�pf�jl neve
	 * @return A bel�lt�tt BufferedImage k�pt�pus
	 */
	BufferedImage load(String filename) {
		BufferedImage img;
		try {
			img = ImageIO.read(new File(filename));
		} 
		catch (IOException e) {
			return null;
		}
		return img;
	}


}


/**
 * Control oszt�ly, itt az ir�ny�t�s ker�l meval�s�t�sra
 * @author      B�lint B�k�si <Bekesi.Balint @ stud.u-szeged.hu>
 */
class Control {
	
	/** ablak kezel�se, csak a bez�r�s opci�t r�gz�tettem itt */
	class wa extends WindowAdapter {
		public void windowClosing(WindowEvent e) {
			System.exit(0);
		}
	}
	
	/** az eg�r kezel�se */
	class ma extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			System.out.println("jujjj");
		}
	}
	
	/* egy�b ... */

	/** 
	 * konstruktor 
	 * @param f A megfelel� JFrame objektum, enn�l kell vizsg�lni 
	 * az ir�ny�t�st  
	 */
	public Control(JFrame f) {
		f.addWindowListener(new wa());
		f.addMouseListener(new ma());
	}
}


/**
 * class Player { ... }
 * @author      B�lint B�k�si <Bekesi.Balint @ stud.u-szeged.hu>
 */
 class Player {
	
	protected String name;
	public int pontszam;
	private LinkedList <Kartyalap>kezben = new LinkedList<Kartyalap>();
	
	public void KezbeHelyez( Kartyalap mit ) {
		kezben.add(mit);
	}
	
	public Kartyalap KezbolKivesz( Kartyalap mit ) {
		kezben.remove(mit);
		return mit;
	}
	
	public String getName() {
		return name;
	}
	
	/** konstruktor */
	public Player( String newname ) {
		name = newname;
	}
	
}




